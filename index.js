/**
 * Created by Joseph Lum on 3/28/2016 at 1500hrs.
 */
var express = require ("express");  // want to include express in the application
var app = express();                // create an app

app.get("/",
    function(req,res){     // req is what we want to get from browser, res is what we send to browser
        // Status - to be included in every response
        res.status(202);
        // MIME type - to be included in every response
        res.type("text/plain");
        res.send("The time now is " + (new Date()));
    }
);

app.get("/hello",function(req,res){
    res.status(200);
    res.type("text/html");
    res.send("<h1>HELLO</h1>");
});

// 3000 - port number
app.listen(3000, function(){
    console.info("Application started on port 3000");
});

